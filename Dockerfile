#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Convertet_C-F/Convertet_C-F.csproj", "Convertet_C-F/"]
RUN dotnet restore "Convertet_C-F/Convertet_C-F.csproj"
COPY . .
WORKDIR "/src/Convertet_C-F"
RUN dotnet build "Convertet_C-F.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Convertet_C-F.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Convertet_C-F.dll"]