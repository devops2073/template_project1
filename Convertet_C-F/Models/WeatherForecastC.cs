namespace Convertet_C_F
{
    public class WeatherForecastC
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC * 1.8);

    }
}