namespace Convertet_C_F
{
    public class WeatherForecastF
    {
        public DateTime Date { get; set; }

        public int TemperatureF { get; set; }

        public int TemperatureC => (int)((int)(TemperatureF - 32) / 1.8);

    }
}