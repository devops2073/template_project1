﻿using Microsoft.EntityFrameworkCore;

namespace Convertet_C_F.Models
{
    public class ConvertContext : DbContext
    {
        public ConvertContext(DbContextOptions dbContextOptions) : base(dbContextOptions) {
            Database.EnsureCreated();
        }

        public DbSet<HistoryOperation> HistoryOperations { get; set; }

    }
}
