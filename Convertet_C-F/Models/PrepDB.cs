﻿
using Microsoft.EntityFrameworkCore;

namespace Convertet_C_F.Models
{
    public static class PrebDB
    {
        public static WebApplicationBuilder PopulateDB(this WebApplicationBuilder builder)
        {
            using var serviceProvider = builder.Services.BuildServiceProvider(); //Возращает сервис провайдер который реализует интерфейс

            var context = serviceProvider.GetService<ConvertContext>();

            if (context == null)
            {
                throw new Exception("Context не создан");
            }
            //context.Database.EnsureCreated();
            //return builder;
            context.Database.Migrate();
            return builder;
        }
    }
}
