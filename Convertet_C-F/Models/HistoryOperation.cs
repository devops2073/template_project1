﻿using System.ComponentModel.DataAnnotations;

namespace Convertet_C_F.Models
{
    public class HistoryOperation
    {
        [Key]
        public int Id { get; set; }

        public DateTime LogDate { get; set; }

        public string InputParam { get; set; }

        public int OutputResult { get; set; }
    }
}
