using Microsoft.AspNetCore.Mvc;
using Convertet_C_F.Models;
using Convertet_C_F.Logging;

namespace Convertet_C_F.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConvertersController : ControllerBase
    {

        private readonly ConvertContext _context;
        private readonly ILogger <ConvertersController> _logger;
        public ConvertersController(ConvertContext context, ILogger <ConvertersController> logger)
        {
            _context = context;
            _logger = logger;
        }


        [HttpGet(template: "ConvertCF/{valueC}")]
        public WeatherForecastC ConvertCF(int valueC)
        {
            var result = new WeatherForecastC
            {
                Date = DateTime.Now.ToUniversalTime(),
                TemperatureC = valueC

            };
            var operation = new HistoryOperation();
            operation.LogDate = result.Date;
            operation.InputParam = $"{valueC} convert CF";
            operation.OutputResult = result.TemperatureF;
            _context.Add(operation);
            _context.SaveChanges();
            _logger.Log(LogLevel.Information,$"{operation.LogDate} Successfully completed the convert CF method");
            return result;

        }
        [HttpGet(template: "ConvertFC/{valueF}")]
        public WeatherForecastF ConvertFC(int valueF)
        {
            var result = new WeatherForecastF
            {
                Date = DateTime.Now.ToUniversalTime(),
                TemperatureF = valueF

            };
            var operation = new HistoryOperation();
            operation.LogDate = result.Date;
            operation.InputParam = $"{valueF} convert FC";
            operation.OutputResult = result.TemperatureC;
            _context.Add(operation);
            _context.SaveChanges();
            _logger.Log(LogLevel.Information, $"{operation.LogDate} Successfully completed the convert FC method");
            return result;

        }
    }
}