using Convertet_C_F.Models;
using Microsoft.EntityFrameworkCore;
using Convertet_C_F.Logging;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Logging.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "/logs/log.txt"));
builder.Logging.SetMinimumLevel(LogLevel.Information);
builder.Logging.AddFilter("Microsoft", LogLevel.None);
builder.Logging.AddFilter("Default", LogLevel.Information);
builder.Services.AddHealthChecks()
    .AddNpgSql(builder.Configuration.GetConnectionString("TraceConnectionString"));

//builder.PopulateDB();
builder.Services.AddDbContext<ConvertContext>(opt =>
{
    opt.UseNpgsql(builder.Configuration.GetConnectionString("TraceConnectionString"));
});


var app = builder.Build();
var config = builder.Configuration;
var useswagger = config["UseSwagger"];
//Configure the HTTP request pipeline.
if (useswagger == "true")
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

Func<HttpContext, HealthReport, Task> writeResponse = HealthCheck.WriteResponse;

app.MapHealthChecks("/health", new HealthCheckOptions()
{
    ResponseWriter = writeResponse
});

app.MapControllers();

app.Run();
